////  SUI2_ViewBuildersApp.swift
//  SUI2_ViewBuilders
//
//  Created on 29/03/2021.
//  
//

import SwiftUI

@main
struct SUI2_ViewBuildersApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
